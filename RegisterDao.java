

package com.example.dao;

        import java.sql.Connection;
        import java.sql.PreparedStatement;
        import java.sql.SQLException;
        import com.example.bean.RegisterBean;
        import com.example.util.DBConnection;

public class RegisterDao {
    public String registerUser(RegisterBean registerBean)
    {
        String Name = registerBean.getName();
        Integer Age = registerBean.getAge();
        Integer DOB = registerBean.getDOB();
        String MailId = registerBean.getMailId();
        String Address = registerBean.getAddress();
        String userName = registerBean.getUserName();
        String password = registerBean.getPassword();

        Connection con;
        PreparedStatement preparedStatement;
        try
        {
            con = DBConnection.createConnection();
            String query = "insert into users(Name,Age,DOB,MailId,Address,userName,password) values (?,?,?,?,?,?,?)";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, Name);
            preparedStatement.setString(2, String.valueOf(Age));
            preparedStatement.setString(3, String.valueOf(DOB));
            preparedStatement.setString(4, MailId);
            preparedStatement.setString(5, Address);
            preparedStatement.setString(6, userName);
            preparedStatement.setString(7, password);

            int i= preparedStatement.executeUpdate();

            if (i!=0)
                return "SUCCESS";
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return "Oops.. Something went wrong there..!";
    }
}
