

package com.example.bean;

public class RegisterBean {

    private String Name;
    private Integer Age;
    private Integer DOB;
    private String MailId;
    private String Address;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setName(String Name) {
        this.Name = Name;
    }
    public String getName() {
        return Name;
    }
    public void setAge(Integer Age) {
        this.Age = Age;
    }
    public Integer getAge() {
        return Age;
    }

    public void setDOB(Integer DOB) {
        this.DOB = DOB;
    }
    public Integer getDOB() {
        return DOB;
    }
    public void setMailId(String MailId) {
        this.MailId = MailId;
    }
    public String getMailId() {
        return MailId;
    }
    public void setAddress(String Address) {
        this.Address = Address;
    }
    public String getAddress() {
        return Address;
    }
}
