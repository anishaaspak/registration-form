

package com.example.controller;
        import com.example.bean.RegisterBean;
        import com.example.dao.RegisterDao;

        import java.io.IOException;
        import javax.servlet.ServletException;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;



public class RegisterServlet extends HttpServlet {



    public RegisterServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String Name = request.getParameter("name");
        Integer Age = Integer.valueOf(request.getParameter("age"));
        Integer DOB = Integer.valueOf(request.getParameter("dob"));
        String MailId = request.getParameter("mailid");
        String Address = request.getParameter("address");
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        RegisterBean registerBean = new RegisterBean();

        registerBean.setName(Name);
        registerBean.setAge(Age);
        registerBean.setDOB(DOB);
        registerBean.setMailId(MailId);
        registerBean.setAddress(Address);
        registerBean.setUserName(userName);
        registerBean.setPassword(password);

        RegisterDao registerDao = new RegisterDao();


        String userRegistered = registerDao.registerUser(registerBean);

        if(userRegistered.equals("SUCCESS"))
        {
            request.getRequestDispatcher("/Home.html").forward(request, response);
        }
        else
        {
            request.setAttribute("errMessage", userRegistered);
            request.getRequestDispatcher("/Register.html").forward(request, response);
        }
    }
}
